/*
	© 2017 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module MenuTree
define('Invoicepay.view'
,	[
		'Backbone.CompositeView'
	,	'Backbone.CollectionView'
	,	'pay.tpl'
	,	'Backbone'
	,	'underscore'
	,	'jQuery'
	
	]
,	function(

		BackboneCompositeView
	,	BackboneCollectionView

	,	pay_tpl
	
	,	Backbone
	,	_
	,	jQuery
	
	)
{
	'use strict';
	debugger;
	// @class MenuTree.View Implements the tree menu items that is present in MyAccount. It's a Singleton. @extends Backbone.View
	return Backbone.View.extend({

		// @property {Function} template
		template: pay_tpl
		
	

	/*,	events: {
			'click [data-action="pay"]': 'pay'
		}*/

		// @method initialize
	,	initialize: function ()
		{
			debugger;
		}


		// @method menuClick Note: cant use bootstrap collapse due to the divs introduced by collection view. @param {HTMLEvent} e
	/*,	pay: function (e, synthetic)
		{
			debugger;
		}*/
		
		
		// @method getContext @returns MenuTree.View.Context
	,	getContext: function()
		{
			debugger;
			// @class MenuTree.View.Context
			return {
				// @propery {Array} menuItems
				
			};
			// @class MenuTree.View
		}


	});
});