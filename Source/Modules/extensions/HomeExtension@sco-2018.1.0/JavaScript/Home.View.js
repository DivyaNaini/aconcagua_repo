/*
	© 2017 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module Home
define(
	'Home.View'
,	[
		'SC.Configuration'
	,	'Utilities.ResizeImage'

	,	'home.tpl'

	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	,	'Utils'
	]
,	function (
		Configuration

	,	resizeImage
	,	home_tpl

	,	Backbone
	,	jQuery
	,	_
	,	Utils
	)
{
	'use strict';

	//@module Home.View @extends Backbone.View
	return Backbone.View.extend({

		template: home_tpl

	,	title: _('Welcome to the store').translate()

	,	page_header: _('Welcome to the store').translate()

	,	attributes: {
			'id': 'home-page'
		,	'class': 'home-page'
		}

	,	events:
		{
			'click [data-action=slide-carousel]': 'carouselSlide'
			
		,	'click [data-action=link]': 'link'
		,	'click [data-action=linkToTop]': 'linkToTop'
		//,	'click [data-action=select]': 'itemselect'
	
		}

	,	initialize: function ()
		{
			var self = this;
			this.windowWidth = jQuery(window).width();
			this.on('afterViewRender', function()
			{
				_.initBxSlider(self.$('[data-slider]'), {
					nextText: '<a class="home-gallery-next-icon"></a>'
				,	prevText: '<a class="home-gallery-prev-icon"></a>'
				});
			});

			var windowResizeHandler = _.throttle(function ()
			{
				if (_.getDeviceType(this.windowWidth) === _.getDeviceType(jQuery(window).width()))
				{
					return;
				}
				this.showContent();

				_.resetViewportWidth();

				this.windowWidth = jQuery(window).width();

			}, 1000);

			this._windowResizeHandler = _.bind(windowResizeHandler, this);

			jQuery(window).on('resize', this._windowResizeHandler);
		}

	,	destroy: function()
		{
			Backbone.View.prototype.destroy.apply(this, arguments);
			jQuery(window).off('resize', this._windowResizeHandler);
		}
	/*,	itemselect: function(e)
		{
			debugger;
			var arr1= [];
			var arr2=[];
			//$.getJSON('http://aconcagua.com/api/items?fieldset=search&id=2055,2058,2059,2060,2061', function (response, request){

			$.ajax({url: "http://aconcagua.com/api/items?fieldset=search&id=2055,2058,2059,2060,2061", success: function(response){

			debugger;
       	var s=[];

       
       	
        	for(var i=0;i<response.items.length;i++){
       		arr1[i]=response.items[i].displayname;
       		arr2[i]= response.items[i].internalid;
       		s[i]=response.items[i].itemimages_detail.urls[0].url;
       		debugger;
       		
       		$(".details").append("<img src = \""+s[i]+"\"><li>"+arr1[i]+"</li><li>"+arr2[i]+"</li>  ");
       	

       	
       	}
       		
	 }});
			
			}*/
	,	link: function(e)
		{
			var link = e.currentTarget.value;
			//console.log("link" + link);
			//document.getElementById("linktotop").scrollIntoView();
			if(link == "1")
			{
			document.querySelector("#carousel").scrollIntoView();
			}
			else if(link == "2")
			{
			document.querySelector("#bottomBanner").scrollIntoView();	
			}
			else if(link == "3")
			{
			document.querySelector("#linktobottom").scrollIntoView();
			}
			//this.$("#linktotop").scrollIntoView();
/*			$("a[rel='m_PageScroll2id']").mPageScroll2id({
    			layout:"auto"
				});*/
			
			 //window.location = "#lines";
			//$("#linktotop").scroll();
			// $(document).scroll($("#linktotop"));
		}
	,	linkToTop: function()
		{
			console.log("link");
			 $(document).scrollTop(0);
		}

		// @method getContext @return Home.View.Context
	,	getContext: function()
		{
			var carouselImages = _.map(Configuration.get('home.carouselImages', []), function(url)
			{
				return Utils.getAbsoluteUrlOfNonManagedResources(url);
			});

			var bottomBannerImages = _.map(Configuration.get('home.bottomBannerImages', []), function(url)
			{
				return Utils.getAbsoluteUrlOfNonManagedResources(url);
			});


			var limit = 100 
			, offset = 0 
			, itemCount = 0
			, noImages = []
			, locale = SC.ENVIRONMENT.currentLanguage.locale.split("_");
			debugger;
			var url = SC.ENVIRONMENT.siteSettings.touchpoints.home
			+ '/api/items'
			+ '?q=' + '' 
			+ '&language=' + locale[0]
			+ '&country=' + locale[1]
			+ '&currency=' + SC.ENVIRONMENT.currentCurrency.code
			+ '&c=' + SC.ENVIRONMENT.companyId
			+ '&limit=' + limit
			+ '&fields=' + 'itemid,itemimages_detail,itemoptions_detail'
			+ '&timestamp=' + Date.now();
			console.log(url);
			debugger;
			jQuery.ajax(url).then(

			function processResults (data)
			{
			debugger;

			data.items.forEach(function (item)
			{

			if (_.isEmpty(item.itemimages_detail))
			{
			noImages.push(item.itemid + ' (all colors)')
			}

			else
			{

			item.itemoptions_detail.fields && item.itemoptions_detail.fields.forEach(function (field)
			{

			if (field.label == 'Color')
			{

			field.values.forEach(function (value)
			{

			if (value.internalid && !item.itemimages_detail.media[value.label])
			{
			noImages.push(item.itemid + ' (' + value.label + ')')
			}
			});
			}
			});
			}
			});
			}
			);
			
			console.log(noImages);



			return {
				// @class Home.View.Context
				// @property {String} imageResizeId
				imageHomeSize: Utils.getViewportWidth() < 768 ? 'homeslider' : 'main'
				// @property {String} imageHomeSizeBottom
			,	imageHomeSizeBottom: Utils.getViewportWidth() < 768 ? 'homecell' : 'main'
				// @property {Array} carouselImages
			,	carouselImages: carouselImages
				// @property {Array} bottomBannerImages
    		,	bottomBannerImages: bottomBannerImages
    			// @class Home.View
			};
		}

	});



});
